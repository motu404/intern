# coding:utf-8
from bottle import run, route, template, get, request

# トップページ
@route("/")
def store_order():#関数を宣言
    return template('store_order') #templateで何かしらしたものを返す

# プロセスの起動
if __name__ == "__main__":
    run(host='127.0.0.1', port=8080, reloader=True, debug=True)